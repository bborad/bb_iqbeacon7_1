﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iqBeaconv7_1.ReaderLayer.Tag;

namespace iqBeaconv7_1.LogicLayer.LogicBlock
{
    public interface ILogic : ILocationConfig
    {

        BlockingCollection<IRampActiveTag> LogicBuffer { get; set; }
        
        void ProcessLogicBuffer();

        
    }
}
