﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Threading;
using System.Threading.Tasks;
using iqBeacon7_1.Logger;
using iqBeaconv7_1.LogicLayer.ConfigFile;
using iqBeaconv7_1.LogicLayer.LogicBlock;
using iqBeaconv7_1.ReaderLayer;
using iqBeaconv7_1.ReaderLayer.Connection;
using iqBeaconv7_1.ReaderLayer.IdentecBus;
using iqBeaconv7_1.ReaderLayer.Tag;

namespace iqBeaconv7_1.LogicLayer
{
    public class LogicBlockManager
    {
        public ConnectionManager ConnManager { get; set; }

        private IList<ILogic> LogicBlockList { get; set; }

        public LogicBlockManager()
        {
            LogicBlockList = new List<ILogic>();
        }

        public void InitReadForward()
        {
            if (ConnManager != null)
                ConnManager.ForwardReadEvent += ConnManager_ForwardReadEvent;

        }

        private void ConnManager_ForwardReadEvent(object sender, RampTagReadEventArg e)
        {
            //var msg = string.Format("Reader # {0}. Tag# {1} Script# {2}", (sender as IdentecBusConnection).ConnectedReader.SerialNumber.ToString(),
            //    e.GetRampTags().Count(), "blah");
            //Console.WriteLine(msg);

            try
            {
                foreach (var ramptag in e.GetRampTags())
                {
                    // filter out the tags with New Loop time as Min value [they are tags that are incorrectly marked back in history]
                    if (ramptag.NewLoopTime == DateTime.MinValue) continue;

                    //Console.WriteLine("Write to Buffer :" + ramptag.NewLoopId);
                    var logicBlock = LogicBlockList.FirstOrDefault(x => x.Id == ramptag.NewLoopId);
                    if (logicBlock == null) return;
                    // Add the new tag to the FIFO queue to be processed by the logic block
                    logicBlock.LogicBuffer.Add(ramptag);
                }
            }
            catch (Exception ex)
            {
                // add to log and suppress the exception    
                this.AddToLog(IqBeaconLogType.Error, string.Format("Error adding Tag to Queue. Exception is {0}", ex));
            }

        }

        public void InitialiseLogicBlock()
        {
            // initialise the location logic blocks from the config file
            this.AddToLog(IqBeaconLogType.Info, string.Format("Initialising Location Logic blocks from config file."));
            var locationList = LocationConfiguration.GetLocationLogicConfiguration();

            foreach (var locationConfig in locationList)
            {
                var logicBlock = new LogicProcessor
                {
                    ArriveThreshold = locationConfig.ArriveThreshold,
                    DepartThreshold = locationConfig.DepartThreshold,
                    NoiseThreshold = locationConfig.NoiseThreshold,
                    Id = locationConfig.Id,
                    MarkerSerialNumber = locationConfig.MarkerSerialNumber,
                    LogicBuffer = new BlockingCollection<IRampActiveTag>()
                };
                
                logicBlock.Start();

                LogicBlockList.Add(logicBlock);

            }
        }

        public void AllocateLogicThreads()
        {
            if (LogicBlockList == null)
            {
                this.AddToLog(IqBeaconLogType.Fatal, string.Format("There are no location logic blocks in the config file. Exiting system..."));
                return;
            }

            for (int i = 0; i < LogicBlockList.Count; i++)
            {
                // use this variable to manage thread safety
                var countRef = i;
                //this.AddToLog(IqBeaconLogType.Info, string.Format("Creating Location Logic Task for {0}...", LogicBlockList[countRef].MarkerSerialNumber));

                var logicTask = Task.Factory.StartNew(() => LogicBlockList[countRef].ProcessLogicBuffer());

                //this.AddToLog(IqBeaconLogType.Info, string.Format("Starting Location Logic thread for {0}...", LogicBlockList[countRef].MarkerSerialNumber));

            }


        }

    }
}
