﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iqBeaconv7_1.LogicLayer
{
    public interface ILocationConfig
    {
        /// <summary>
        /// id of the Location Module
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// Time Threshold (in milliseconds) to reject the arrival tags
        /// </summary>
        TimeSpan ArriveThreshold { get; set; }

        /// <summary>
        /// Time Threshold (in milliseconds) to process the depart tags
        /// </summary>
        TimeSpan DepartThreshold { get; set; }

        /// <summary>
        /// Serial Number of the Position Marker device
        /// </summary>
        string MarkerSerialNumber { get; set; }

        TimeSpan NoiseThreshold { get; set; }
    }
}
