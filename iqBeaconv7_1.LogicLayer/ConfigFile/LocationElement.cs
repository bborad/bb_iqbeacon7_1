﻿using System;
using System.Configuration;

namespace iqBeaconv7_1.LogicLayer.ConfigFile
{
    public class LocationElement : ConfigurationElement, ILocationConfig
    {
        /// <summary>
        /// id of the Location Module
        /// </summary>
        [ConfigurationProperty("Id", IsKey = true, IsRequired = true)]
        public int Id
        {
            get { return (int) base["Id"]; }
            set { base["Id"] = value; }
        }

        /// <summary>
        /// Time Threshold (in milliseconds) to reject the arrival tags
        /// </summary>
        [ConfigurationProperty("arriveThreshold")]
        public TimeSpan ArriveThreshold
        {
            get
            {
                return (TimeSpan)base["arriveThreshold"];
            }
            set
            {
                base["arriveThreshold"] = value;
            }
        }

        /// <summary>
        /// Time Threshold to reject markings as noise Markings generally much higher than arrival and departure threshold
        /// [NOTE : this should be calculated by taking into account congestion and latency as we do not want to ignore valid packets]
        /// </summary>
        [ConfigurationProperty("noiseThreshold")]
        public TimeSpan NoiseThreshold
        {
            get
            {
                return (TimeSpan)base["noiseThreshold"];
            }
            set
            {
                base["noiseThreshold"] = value;
            }
        }

        /// <summary>
        /// Time Threshold (in milliseconds) to process the depart tags
        /// </summary>
        [ConfigurationProperty("departThreshold")]
        public TimeSpan DepartThreshold
        {
            get
            {
                return (TimeSpan)base["departThreshold"];
            }
            set
            {
                base["departThreshold"] = value;
            }
        }

        /// <summary>
        /// Serial Number of the Position Marker device
        /// </summary>
        [ConfigurationProperty("markerSerialNumber")]
        public string MarkerSerialNumber
        {
            get
            {
                return (string)base["markerSerialNumber"];
            }
            set
            {
                base["markerSerialNumber"] = value;
            }
        }
    }
}
