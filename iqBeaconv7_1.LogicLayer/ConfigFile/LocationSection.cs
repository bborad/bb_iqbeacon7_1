﻿using System.Configuration;

namespace iqBeaconv7_1.LogicLayer.ConfigFile
{
    public class LocationSection : ConfigurationSection
    {
        [ConfigurationProperty("locations", IsRequired = true, IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(LocationCollection), AddItemName = "location")]
        public LocationCollection Locations
        {
            get { return (LocationCollection)this["locations"]; }
            set { this["locations"] = value; }
        }
        
    }
}
