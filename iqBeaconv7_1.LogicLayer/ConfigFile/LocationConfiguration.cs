﻿using System.Collections.Generic;

namespace iqBeaconv7_1.LogicLayer.ConfigFile
{
    public static class LocationConfiguration
    {
        public static IList<ILocationConfig> GetLocationLogicConfiguration()
        {
            var locationBlocks = new List<ILocationConfig>();

            // ReSharper disable once SuggestUseVarKeywordEvident
            var section = (LocationSection)System.Configuration.ConfigurationManager.GetSection("locationConfig");
            if (section != null)
            {
                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (LocationElement element in section.Locations)
                {
                    locationBlocks.Add(new LocationElement { Id = element.Id, MarkerSerialNumber = element.MarkerSerialNumber, ArriveThreshold = element.ArriveThreshold, DepartThreshold = element.DepartThreshold, NoiseThreshold = element.NoiseThreshold});
                }
            }

            return locationBlocks;
        }
    }
}
