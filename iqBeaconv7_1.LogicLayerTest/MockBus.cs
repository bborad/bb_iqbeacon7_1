﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using iqBeaconv7_1.LogicLayer.LogicBlock;
using iqBeaconv7_1.ReaderLayer.Tag;

namespace iqBeaconv7_1.LogicLayerTest
{
    public class MockVehicle
    {
        public uint BusNumber { get; set; }

        public IList<RampActiveTag> DriveOutsideInterrogationZone(TimeSpan timeSpan)
        {
            return null;
        }

        public List<IRampActiveTag> ArriveAtStand(LogicProcessor standLogic, DateTime startTime)
        {
            var currentTime = startTime;
            var tagList = new List<IRampActiveTag>();
            for (int i = 0; i < standLogic.ArriveThreshold.Seconds * 3; i++)
            {
                currentTime = currentTime.AddMilliseconds(500);
                IRampActiveTag tag = new RampActiveTag();
                tag.SerialNumber = BusNumber;
                tag.NewLoopId = standLogic.Id;
                tag.NewLoopTime = currentTime;
                tag.TimeFirstSeen = currentTime;
                tag.TimeLastSeen = currentTime;

                tagList.Add(tag);
            }
            return tagList;
        }

        public void RestAtStand(DateTime startTime, LogicProcessor standLogic, int seconds)
        {
            var currentTime = startTime;
            var tagList = new List<IRampActiveTag>();
            for (int i = 0; i < seconds * 2; i++)
            {
                currentTime = currentTime.AddSeconds(0.5);
                IRampActiveTag tag = new RampActiveTag();
                tag.SerialNumber = BusNumber;
                tag.NewLoopId = standLogic.Id;
                tag.NewLoopTime = currentTime;
                tag.TimeFirstSeen = currentTime;
                tag.TimeLastSeen = currentTime;

                tagList.Add(tag);
            }
        }

        public void DepartFromStand()
        {

        }

        public IList<IRampActiveTag> DriveByStand(DateTime startTime, LogicProcessor standLogic)
        {
            var currentTime = startTime;
            var tagList = new List<IRampActiveTag>();
            for (int i = 0; i < standLogic.ArriveThreshold.Seconds - 1; i++)
            {
                currentTime = currentTime.AddSeconds(1);
                IRampActiveTag tag = new RampActiveTag();
                tag.SerialNumber = BusNumber;
                tag.NewLoopId = standLogic.Id;
                tag.NewLoopTime = currentTime;
                tag.TimeFirstSeen = currentTime;
                tag.TimeLastSeen = currentTime;

                tagList.Add(tag);
            }
            return tagList;
        }

        public void ArriveAtTerminal()
        {

        }

        public void DepartFromTerminal()
        {

        }
    }
}
