﻿/* Name - IqBeaconLogType
 * Author - Puru Karki
 * Purpose - Expose the logger type for IqBeaconLogger
 * Created Date - 15/10/2014
*/
namespace iqBeacon7_1.Logger
{
    public enum IqBeaconLogType
    {
        /// <summary>
        /// Warning : this is a system only event, do not use this
        /// </summary>
        Off = 1,
        Fatal = 2,
        Error = 3,
        Warn = 4,
        Info = 5,
        Debug = 6,
        /// <summary>
        /// Warning : this is a system only event, do not use this
        /// </summary>
        All = 7
    }
}