﻿/* Name - IqBeaconLogger
 * Author - Puru Karki
 * Purpose - Logger for IqBeacon  
 * Created Date - 19/10/2014
*/
using System;
using System.Globalization;
using log4net;

namespace iqBeacon7_1.Logger
{
    public static class IqBeaconLogger
    {

        /// <summary>
        /// Write to Reader log
        /// WARNING : Only classes that implement IReader making this call will work successfully
        /// </summary>
        /// <param name="logObject"></param>
        /// <param name="logType"></param>
        /// <param name="message"></param>
        public static void AddToReaderLog(this object logObject, IqBeaconLogType logType, string message)
        {
            // get IReader interface, if implemented
            var type = logObject.GetType().GetInterface("IReader");
            if (type == null) return;
            // write to log file
            WriteLog(type, logType, message);
        }

        /// <summary>
        /// Write the message to log file
        /// </summary>
        /// <param name="logObjectType"></param>
        /// <param name="logType"></param>
        /// <param name="message"></param>
        private static void WriteLog(Type logObjectType, IqBeaconLogType logType, string message)
        {
            ILog log = LogManager.GetLogger(logObjectType);

            var msg = string.Format(" {0} - {1}",
                             DateTime.Now.ToString(CultureInfo.GetCultureInfo("en-AU")), message);
            switch (logType)
            {
                case IqBeaconLogType.Off:
                    {
                        // this is off limits, so ignore
                        break;
                    }
                case IqBeaconLogType.Fatal:
                    {
                        // generate and log Fatal event message

                        log.Fatal(msg);
                        break;
                    }
                case IqBeaconLogType.Error:
                    {
                        // generate and log Error event message
                        log.Error(msg);
                        break;
                    }
                case IqBeaconLogType.Warn:
                    {
                        // generate and log warning message
                        log.Warn(msg);
                        break;
                    }
                case IqBeaconLogType.Info:
                    {
                        // generate and log info message
                        log.Info(msg);
                        break;
                    }
                case IqBeaconLogType.Debug:
                    {
                        log.Debug(msg);
                        break;
                    }
                case IqBeaconLogType.All:
                    {
                        // this is off limits, so ignore
                        break;
                    }
            }
        }

        /// <summary>
        /// Write object to the respective log file, if configured in log4net.config file
        /// </summary>
        /// <param name="logObject"></param>
        /// <param name="logType"></param>
        /// <param name="message"></param>
        public static void AddToLog(this object logObject, IqBeaconLogType logType, string message)
        {
            WriteLog(logObject.GetType(), logType, message);
        }
    }
}
