﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iqBeaconv7_1.ReaderLayer.Tag
{
    public enum RampBatteryStatus
    {
        Poor = 0,
        Good = 1,
        Indeterminate = 2,
        Unknown = 3
    }
}
