﻿/* Name - ConnectionManager
 * Author - Puru Karki
 * Purpose - Manage connections to the bus the threads for readers to run 
 * Created Date - 22/10/2014
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using iqBeacon7_1.Logger;
using iqBeaconv7_1.ReaderLayer.Connection;
using iqBeaconv7_1.ReaderLayer.ConfigFile;
using iqBeaconv7_1.ReaderLayer.IdentecBus;

namespace iqBeaconv7_1.ReaderLayer
{
    public class ConnectionManager
    {
        /// <summary>
        /// Identec Bus list
        /// </summary>
        private List<IReaderBus> IdentecBusList { get; set; }

        /// <summary>
        /// propagate the read event from the bus to the logic block
        /// </summary>
        public event EventHandler<RampTagReadEventArg> ForwardReadEvent;

        private string TestString { get; set; }

        public ConnectionManager()
        {
            IdentecBusList = new List<IReaderBus>();
            TestString = "Hello Threads";
        }

        /// <summary>
        /// Initialise Identec Bus from the config file
        /// </summary>
        public void InitialiseIdentecBus()
        {
            // initialise the Generic Bus from the config file
            this.AddToLog(IqBeaconLogType.Info, string.Format("Initialising Devices from config file."));
            // get Generic Bus from the file
            IList<IBusConfig> genericBus = ReaderConfiguration.GetBusConfiguration();

            foreach (var bus in genericBus)
            {
                var identecBus = new IdentecBusConnection(bus.Ip, bus.Port, bus.Description);
                // only add new bus, if it does not already exist in the list
                if (IdentecBusList != null && !IdentecBusList.Exists(x => x.Ip.Equals(bus.Ip)))
                {
                    // add this identec bus to the list
                    IdentecBusList.Add(identecBus);
                    this.AddToLog(IqBeaconLogType.Info, string.Format("Added bus {0} from config file.", identecBus.Ip));
                }
            }
        }

        public void AllocateReaderThreads()
        {
            for (int i = 0; i < IdentecBusList.Count; i++)
            {
                // use this variable to manage Thread Safety
                int refCount = i;
                var identecThread = new Thread(() => IdentecBusList[refCount].InitiateRead());

                // subscribe to read event
                IdentecBusList[refCount].ReadMessage += ConnectionManager_ReadMessage;

                this.AddToLog(IqBeaconLogType.Info, string.Format("Starting connector thread for bus {0}.", IdentecBusList[refCount].Ip));
                identecThread.Start();
            }
        }

        public virtual void ConnectionManager_ReadMessage(object sender, RampTagReadEventArg rampTagReadEventArg)
        {
            if (ForwardReadEvent != null)
            {
                ForwardReadEvent(sender, rampTagReadEventArg);
            }
        }
    }
}
