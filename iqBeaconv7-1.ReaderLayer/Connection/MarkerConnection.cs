﻿/* Name - PortMbConnection
 * Author - Puru Karki
 * Purpose - Position Marker object shell only [TODO - requirements are not clear yet]
 * Created Date - 22/10/2014
*/
using IDENTEC.PositionMarker;

namespace iqBeaconv7_1.ReaderLayer.Connection
{
    public class MarkerConnection : IMarker
    {
        private PositionMarker MarkerObj { get; set; }

        public MarkerConnection(PositionMarker marker)
        {
            MarkerObj = marker;
            
        }
    }
}
