﻿/* Name - PortMbConnection
 * Author - Puru Karki
 * Purpose - To configure, connect and read from the Identec iPortMB Reader
 * Created Date - 22/10/2014
*/
using System.Collections.Generic;
using IDENTEC.Readers.BeaconReaders;
using iqBeaconv7_1.ReaderLayer.Tag;

namespace iqBeaconv7_1.ReaderLayer.Connection
{
    public class PortMbConnection : IReader
    {

        /// <summary>
        /// Identec iPortMB Reader device
        /// </summary>
        private iPortMB IPortMbObj { get; set; }

        /// <summary>
        /// Reader Serial Number
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// Flag to check if the reader is connected and ready to read from the Identec Device
        /// </summary>
        public bool IsReaderConnected { get; private set; }


        public PortMbConnection(iPortMB reader)
        {
            IPortMbObj = reader;
            SerialNumber = reader.SerialNumber;
        }

        /// <summary>
        /// Connect to the device
        /// </summary>
        public void Connect()
        {
            IsReaderConnected = true;
        }

        /// <summary>
        /// Disconnect from the device
        /// </summary>
        public void Disconnect()
        {
            IsReaderConnected = false;
        }

        /// <summary>
        /// Read Tags from the bus using the iPortMB Reader
        /// </summary>
        /// <returns></returns>
        public IList<IRampActiveTag> ReadBuffer()
        {
            if (IPortMbObj != null && IPortMbObj.DataStream.IsOpen)
            {
                var data = IPortMbObj.GetTags(true);
                var rampTagList = Utility.IdenteciB2LTagToTag(this, data);
                return rampTagList;
            }
            return null;
        }





    }
}
