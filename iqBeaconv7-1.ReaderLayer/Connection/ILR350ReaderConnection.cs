﻿/* Name - Ilr350ReaderConnection
 * Author - Puru Karki
 * Purpose - To configure, connect and read from the Identec ILR350Reader Reader
 * Created Date - 22/10/2014
*/
using System;
using System.Collections.Generic;
using IDENTEC.ILR350.Readers;
using iqBeacon7_1.Logger;
using iqBeaconv7_1.ReaderLayer.Tag;


namespace iqBeaconv7_1.ReaderLayer.Connection
{
    public class Ilr350ReaderConnection : IReader
    {
        /// <summary>
        /// Get Set the ILR350Reader object managed by this connection
        /// </summary>
        private ILR350Reader Ilr350ReaderObj { get; set; }

        /// <summary>
        /// Get Set Reader Serial Number
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// Get Flag to check if the reader is initialised and ready to read from the bus
        /// </summary>
        public bool IsReaderConnected { get; private set; }

        public Ilr350ReaderConnection(ILR350Reader reader)
        {
            Ilr350ReaderObj = reader;
            SerialNumber = reader.SerialNumber;
        }

        /// <summary>
        /// Connect the Reader
        /// </summary>
        public void Connect()
        {
            IsReaderConnected = true;
        }

        /// <summary>
        /// Disconnect the reader
        /// </summary>
        public void Disconnect()
        {
            IsReaderConnected = false;
        }

        /// <summary>
        /// Read Tags from the Ilr350Reader
        /// </summary>
        /// <returns></returns>
        public IList<IRampActiveTag> ReadBuffer()
        {
            this.AddToLog(IqBeaconLogType.Info, string.Format("Reader# {0} starting read on {1}", SerialNumber, DateTime.Now));
            if (Ilr350ReaderObj != null && Ilr350ReaderObj.DataStream.IsOpen)
            {
                var data = Ilr350ReaderObj.GetBeaconTags();
                // convert Identec Tags to Ramp Tags
                var rampTagList = this.Identec350TagToTag(data);
                return rampTagList;
            }
            return null;
        }

    }
}
