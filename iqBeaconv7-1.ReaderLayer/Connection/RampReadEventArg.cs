﻿/* Name - PortMbConnection
 * Author - Puru Karki
 * Purpose - To collect Ramp Tag Reads from the Reader
 * Created Date - 20/10/2014
*/
using System;
using System.Collections.Generic;
using iqBeaconv7_1.ReaderLayer.Tag;

namespace iqBeaconv7_1.ReaderLayer.Connection
{
    public class RampTagReadEventArg : EventArgs
    {
        /// <summary>
        /// Ramp Tag list
        /// </summary>
        private IList<IRampActiveTag> RampTags { get; set; }

        /// <summary>
        /// Constructor to initialise the Ramp Tag list
        /// </summary>
        /// <param name="tags"></param>
        public RampTagReadEventArg(IList<IRampActiveTag> tags)
        {
            RampTags = tags;
        }

        /// <summary>
        /// Get Ramp tags list
        /// </summary>
        /// <returns></returns>
        public IList<IRampActiveTag> GetRampTags()
        {
            return RampTags;
        }

    }
}
