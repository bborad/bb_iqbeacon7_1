﻿/* Name - Utility
 * Author - Puru Karki
 * Purpose - Utility function collection [TODO - requirements are not clear yet]
 * Created Date - 20/10/2014
*/
using System.Collections.Generic;
using IDENTEC.Readers;
using iqBeacon7_1.Logger;
using iqBeaconv7_1.ReaderLayer.Tag;
using IDENTEC.ILR350.Tags;
using IDENTEC.Utilities.BeaconData;
using IDENTEC.Tags;
using IDENTEC.Tags.BeaconTags;
using System;
namespace iqBeaconv7_1.ReaderLayer.Connection
{
    public static class Utility
    {
        /// <summary>
        /// Translates Identec350 tag to Ramp Active tag
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="identecTags"></param>
        /// <returns></returns>
        public static IList<IRampActiveTag> Identec350TagToTag(this IReader reader, ILR350TagCollection identecTags)
        {
            var tagList = new List<IRampActiveTag>();
            foreach (ILR350Tag t in identecTags)
            {
                try
                {

                    var tagRead = new RampActiveTag();

                    tagRead.BeaconCounterHighByte = t.BeaconCounterHighByte;
                    tagRead.BeaconCounterLowByte = t.BeaconCounterLowByte;
                    tagRead.SerialNumber = t.SerialNumber;
                    tagRead.TimeFirstSeen = t.TimeFirstSeen;
                    tagRead.TimeLastSeen = t.TimeLastSeen;


                    try
                    {
                        List<BeaconInfo> infos = BeaconDataConverter.Convert(t);
                        LFMarker marker = infos.Find(x => x is LFMarker) as LFMarker;

                        if (marker != null)
                        {
                            if (marker.NewerPosition.PositionTime > DateTime.MinValue)
                            {
                                // do something
                            }
                            tagRead.NewLoopId = marker.NewerPosition.LoopID;
                            tagRead.OldLoopId = marker.OlderPosition.LoopID;
                            tagRead.NewLoopTime = marker.NewerPosition.PositionTime;
                            tagRead.OldLoopTime = marker.OlderPosition.PositionTime;

                        }
                    }
                    catch
                    {
                        continue;
                    }

                    reader.AddToReaderLog(IqBeaconLogType.Info,tagRead.ToCsv());
                    tagList.Add(tagRead);
                }
                catch
                {
                    continue;
                }
            }

            return tagList;
        }

        public static IList<IRampActiveTag> IdenteciB2LTagToTag(this IReader reader, TagCollection tagCollection)
        {
            IList<IRampActiveTag> tagList = new List<IRampActiveTag>();
            foreach (iB2Tag t in tagCollection)
            {
                IRampActiveTag tag = new RampActiveTag();

                tag.Battery = GetBatteryStatus(t.Battery);
                tag.TimeLastSeen = t.ContactTime;
                tag.TimeFirstSeen = t.FirstSeen;
                tag.BeaconCounterHighByte = t.HighByteAgeCount;
                tag.BeaconCounterLowByte = t.LowByteAgeCount;
                tag.SerialNumber = t.Number;

                //Get Loop Data from Tag
                try
                {
                    if (t.LoopData != null)
                    {
                        tag.NewLoopTime = t.LoopData.NewerPosition.PositionTime;
                        tag.OldLoopTime = t.LoopData.OlderPosition.PositionTime;
                    }
                }
                catch
                {
                    continue;
                }
                reader.AddToLog(IqBeaconLogType.Info,tag.ToCsv());
                tagList.Add(tag);
            }

            return tagList;
        }

        /// <summary>
        /// Get Ramp Battery Status
        /// </summary>
        /// <param name="batteryStatus"></param>
        /// <returns></returns>
        private static RampBatteryStatus GetBatteryStatus(IDENTEC.Tags.BatteryStatus batteryStatus)
        {
            switch (batteryStatus)
            {
                case IDENTEC.Tags.BatteryStatus.Good:
                    return RampBatteryStatus.Good;
                case IDENTEC.Tags.BatteryStatus.Indeterminate:
                    return RampBatteryStatus.Indeterminate;
                case IDENTEC.Tags.BatteryStatus.Poor:
                    return RampBatteryStatus.Poor;
            }
            return RampBatteryStatus.Unknown;
        }

        

    }
}
