﻿/* Name - Ilr350ReaderConnection
 * Author - Puru Karki
 * Purpose - To configure, connect and read from the Identec iPortM350 Reader
 * Created Date - 22/10/2014
*/
using System.Collections.Generic;
using IDENTEC.ILR350.Readers;
using iqBeaconv7_1.ReaderLayer.Tag;

namespace iqBeaconv7_1.ReaderLayer.Connection
{
    public class PortM350Connection : IReader
    {
        /// <summary>
        /// Identec iPort350 Reader device
        /// </summary>
        private iPortM350 IportM350Obj { get; set; }

        /// <summary>
        /// Reader serial number
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// Flag to check if the reader is connected and ready to read from the Identec Device
        /// </summary>
        public bool IsReaderConnected { get; private set; }

        
        public PortM350Connection(iPortM350 reader)
        {
            IportM350Obj = reader;
            SerialNumber = reader.SerialNumber;
        }

        /// <summary>
        /// Connect to the device
        /// </summary>
        public void Connect()
        {
            IsReaderConnected = true;
        }

        /// <summary>
        /// Disconnect from the device
        /// </summary>
        public void Disconnect()
        {
            IsReaderConnected = false;
        }

        /// <summary>
        /// Read tags from the Reader Buffer
        /// </summary>
        /// <returns></returns>
        public IList<IRampActiveTag> ReadBuffer()
        {
            if (IportM350Obj != null && IportM350Obj.DataStream.IsOpen)
            {
                var data = IportM350Obj.GetBeaconTags();
                var rampTagList = this.Identec350TagToTag(data);
                    return rampTagList;
            }
            return null;
        }
    }
}
