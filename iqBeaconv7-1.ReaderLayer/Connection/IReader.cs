﻿/* Name - IReader
 * Author - Puru Karki
 * Purpose - Define interface requirements for a Reader
 * Created Date - 15/10/2014
*/

using System.Collections.Generic;
using iqBeaconv7_1.ReaderLayer.Tag;

namespace iqBeaconv7_1.ReaderLayer.Connection
{
    public interface IReader
    {
        /// <summary>
        /// Flag to check if the reader is connected and ready to receive tags from the bus
        /// </summary>
        bool IsReaderConnected { get; }

        /// <summary>
        /// Connect to the reader
        /// </summary>
        void Connect();

        /// <summary>
        /// disconnect from the reader
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Read the Tags from the reader device
        /// </summary>
        /// <returns></returns>
        IList<IRampActiveTag> ReadBuffer();

        /// <summary>
        /// Reader device serial Number
        /// </summary>
        string SerialNumber { get; set; }
    }
}
