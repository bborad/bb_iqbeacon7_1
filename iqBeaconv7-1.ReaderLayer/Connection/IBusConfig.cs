﻿/* Name - IBusConfig
 * Author - Puru Karki
 * Purpose - To capture the properties of the Communication Bus
 * Created Date - 15/10/2014
*/

namespace iqBeaconv7_1.ReaderLayer.Connection
{
    public interface IBusConfig
    {
        /// <summary>
        /// Get or Set the Ip Address of the communication bus
        /// </summary>
        string Ip { get; set; }
        
        /// <summary>
        /// Get or set the Port of the communication bus
        /// </summary>
        int Port { get; set; }

        /// <summary>
        /// Get of set the description of the communication bus
        /// </summary>
        string Description { get; set; }

    }
}
