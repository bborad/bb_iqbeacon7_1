﻿/* Name - IConnector
 * Author - Puru Karki
 * Purpose - To collect Ramp Tag Reads from the Reader
 * Created Date - 22/10/2014
*/
using System;
using iqBeaconv7_1.ReaderLayer.Connection;

namespace iqBeaconv7_1.ReaderLayer.IdentecBus
{
    public interface IReaderBus
    {
        /// <summary>
        /// IReader device that is connected to this reader bus
        /// </summary>
        IReader ConnectedReader { get; }

        /// <summary>
        /// IMarker device that is connected to this reader bus
        /// </summary>
        IMarker ConnectedMarker { get; }
        
        /// <summary>
        /// Initiate Read from the bus
        /// </summary>
        void InitiateRead();

        /// <summary>
        /// Subscribe to IReader Tag Read event
        /// </summary>
        event EventHandler<RampTagReadEventArg> ReadMessage;

        /// <summary>
        /// Ip of the Bus
        /// </summary>
        string Ip { get; }

        /// <summary>
        /// Port of the Bus
        /// </summary>
        int Port { get; }
    }
}
