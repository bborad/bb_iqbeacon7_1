﻿/* Name - IdentecBusConnection
 * Author - Puru Karki
 * Purpose - To handle connection to reader and/or marker devices and handle Tag Reads from the connected device
 * Created Date - 22/10/2014
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using IDENTEC.ILR350.Readers;
using IDENTEC.PositionMarker;
using IDENTEC.Readers;
using IDENTEC.Readers.BeaconReaders;
using iqBeacon7_1.Logger;
using iqBeaconv7_1.ReaderLayer.Connection;
using IDENTEC;
using iqBeaconv7_1.ReaderLayer.Tag;

namespace iqBeaconv7_1.ReaderLayer.IdentecBus
{
    public class IdentecBusConnection : IReaderBus, IDisposable
    {
        // IP of the bus
        public string Ip { get; private set; }

        // Port of the bus
        public int Port { get; private set; }

        // bus description [optional]
        public string Description { get; private set; }

        /// <summary>
        /// TCP stream used to connect to the Identec Bus
        /// </summary>
        private TCPSocketStream BusStream { get; set; }

        /// <summary>
        /// Overridden Constructor 
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        /// <param name="description"></param>
        public IdentecBusConnection(string ip, int port, string description)
        {
            Description = description;
            Port = port;
            Ip = ip;

        }

        /// <summary>
        /// Dispose the Bus object
        /// </summary>
        public void Dispose()
        {

        }

        /// <summary>
        /// Initialise ConnectedReader and ConnectedMarker devices connected to the bus
        /// </summary>
        /// <returns></returns>
        internal void InitialiseConnectedDevices()
        {
            try
            {
                // get bus Adapter
                var busAdapter = new iBusAdapter(BusStream);

                // get devices connected to the bus
                IBusDevice[] deviceList = busAdapter.EnumerateBusModules();

                // the system shall only connect to one reader in any bus, if more than 1 reader is found, log error
                foreach (var dev in deviceList)
                {
                    if (dev is iPortM350)
                    {
                        InitialiseIPortM350(dev);
                    }
                    else if (dev is ILR350Reader)
                    {
                        InitialiseIlr350Reader(dev);
                    }
                    else if (dev is PositionMarker)
                    {
                        InititalisePositionMarker(dev);
                    }

                    else if (dev is iPortMB)
                    {
                        InitialisePortMb(dev);
                    }

                }

                // no readers connected to the bus, log error
                if (ConnectedReader == null)
                {
                    this.AddToLog(IqBeaconLogType.Error,
                        string.Format("There are no readers connected to bus {0}", Ip));
                }

            }
            catch (SocketException)
            {
                // handle socket exception here with TODO exponential backoff
                Thread.Sleep(10000);
                // restart the connection sequence
                this.AddToLog(IqBeaconLogType.Error,
                    string.Format("Connection could not be establised to the bus {0}. Retrying...", Ip));
                StartInitialisationSequence();
            }
            catch (Exception ex)
            {
                this.AddToLog(IqBeaconLogType.Fatal,
                    string.Format("Caught Unknown Exception for Reader {0} in bus {1}. Terminating Thread... Exception is... {2}",
                        ConnectedReader.SerialNumber, Ip, ex.ToString()));
            }
        }

        /// <summary>
        /// Initialise and Connect to PortMB reader device
        /// </summary>
        /// <param name="dev"></param>
        private void InitialisePortMb(IBusDevice dev)
        {
            if (ConnectedReader == null)
            {
                // initialise reader as iPortMB
                ConnectedReader = new PortMbConnection(dev as iPortMB);
                ConnectedReader.Connect();
                this.AddToLog(IqBeaconLogType.Info,
                    string.Format("{0} reader connected to bus {1}", ConnectedReader.SerialNumber, Ip));
            }
            else
            {
                this.AddToLog(IqBeaconLogType.Error,
                    string.Format("Reader already connected to bus {0}", Ip));
            }
        }

        /// <summary>
        /// Initialise and Connect to PositionMarker device
        /// </summary>
        /// <param name="dev"></param>
        private void InititalisePositionMarker(IBusDevice dev)
        {
            if (ConnectedMarker == null)
            {
                ConnectedMarker = new MarkerConnection(dev as PositionMarker);
                this.AddToLog(IqBeaconLogType.Info,
                    string.Format("{0} Position Marker connected to bus {1}", ConnectedReader.SerialNumber, Ip));
            }
            else
            {
                this.AddToLog(IqBeaconLogType.Error,
                    string.Format("Reader already connected to bus {0}", Ip));
            }
        }

        /// <summary>
        /// Initialise and Connet to Ilr350Reader device
        /// </summary>
        /// <param name="dev"></param>
        private void InitialiseIlr350Reader(IBusDevice dev)
        {
            if (ConnectedReader == null)
            {
                // initialise reader as ILR350Reader
                ConnectedReader = new Ilr350ReaderConnection(dev as ILR350Reader);
                // establish Read Connection to the Reader
                ConnectedReader.Connect();
                this.AddToLog(IqBeaconLogType.Info,
                    string.Format("{0} reader connected to bus {1}", ConnectedReader.SerialNumber, Ip));
            }
            else
            {
                this.AddToLog(IqBeaconLogType.Error,
                    string.Format("Reader already connected to bus {0}", Ip));
            }
        }

        /// <summary>
        /// Initialise and Connect to IPortM350 device
        /// </summary>
        /// <param name="dev"></param>
        private void InitialiseIPortM350(IBusDevice dev)
        {
            if (ConnectedReader == null)
            {
                // initialise reader as PortM350Connection
                ConnectedReader = new PortM350Connection(dev as iPortM350);
                // establish Read Connection to the Reader
                ConnectedReader.Connect();
                this.AddToLog(IqBeaconLogType.Info,
                    string.Format("{0} reader connected to bus {1}", ConnectedReader.SerialNumber, Ip));
            }
            else
            {
                this.AddToLog(IqBeaconLogType.Error,
                    string.Format("Reader already connected to bus {0}", Ip));
            }
        }

        /// <summary>
        /// Configure TCP Stream for connectng to the bus
        /// </summary>
        /// <returns></returns>
        private void ConfigureTcpStream()
        {
            try
            {
                BusStream = new TCPSocketStream(Ip, Port)
                {
                    ReadTimeout = new TimeSpan(0, 5, 0),
                    WriteTimeout = new TimeSpan(0, 5, 0)
                };

                // open stream for reads
                if (!BusStream.IsOpen) BusStream.Open();
            }
            catch (SocketException ex)
            {
                this.AddToLog(IqBeaconLogType.Error,
                    string.Format("Failed to open socket in bus {0}. Exception is {1}. Retrying with exp wait...",
                        Ip, ex.ToString()));
            }

        }

        /// <summary>
        /// Disconnect the stream and dispose socket object
        /// </summary>
        public void DisconnectAndCloseStream()
        {

            if (BusStream != null)
            {
                if (BusStream.IsOpen)
                    BusStream.Close();
                BusStream.Dispose();
            }
        }

        /// <summary>
        /// IReader device that is connected toe the bus
        /// </summary>
        public IReader ConnectedReader { get; private set; }

        /// <summary>
        /// IMarker device that is connected to the bus
        /// </summary>
        public IMarker ConnectedMarker { get; private set; }


        /// <summary>
        /// Read tag information
        /// </summary>
        public void InitiateRead()
        {
            try
            {
                this.AddToLog(IqBeaconLogType.Info,
                    string.Format("Initiating Read from reader in bus {0}... ", Ip));
                while (true)
                {
                    // first check if the device is connected
                    if (ConnectedReader != null && ConnectedReader.IsReaderConnected)
                    {
                        // read buffer from the reader
                        IList<IRampActiveTag> rampTagList = ConnectedReader.ReadBuffer();
                        if (rampTagList != null && rampTagList.Any())
                        {
                            // trigger read event handler
                            if (ReadMessage != null)
                                ReadMessage(this, new RampTagReadEventArg(rampTagList));
                        }
                    }
                    else
                    {
                        // the device is not connected
                        // it needs to go through the initialisation sequence
                        StartInitialisationSequence();
                    }
                }
            }
            catch (SocketException)
            {
                // handle socket exception here with TODO exponential backoff
                Thread.Sleep(10000);
                // restart the connection sequence
                this.AddToLog(IqBeaconLogType.Error,
                    string.Format("Reader failed while reading tags due to Socket Exception in bus {0}. Retrying...",
                        Ip));
                // start the initialisation sequence to reconnect the bus
                StartInitialisationSequence();
            }
            catch (ReaderTimeoutException)
            {
                // handle socket exception here with TODO exponential backoff
                Thread.Sleep(10000);
                // restart the connection sequence
                this.AddToLog(IqBeaconLogType.Error,
                    string.Format("Reader failed while reading tags due to ReaderTimeout Exception in bus {0}. Retrying...",
                        ConnectedReader.SerialNumber));
                // start the initialisation sequence to reconnect the bus
                StartInitialisationSequence();
            }
            catch (Exception ex)
            {
                this.AddToLog(IqBeaconLogType.Fatal,
                    string.Format("Caught Unknown Exception in bus {0}. Terminating Thread... Exception is... {1}",
                         Ip, ex));
            }

        }

        /// <summary>
        /// Initialisation Sequence to connect to the bus
        /// </summary>
        private void StartInitialisationSequence()
        {
            // set reader mode to disconnected
            if (ConnectedReader != null)
            {
                // if a reader is already connected, then disconnect the reader and null it
                // as a new reader will need to be connected when the connection is reset
                ConnectedReader.Disconnect();
                ConnectedReader = null;

            }

            // close and dispose any open TCP connection stream
            DisposeTcpStream();

            // configure and open new TCP stream
            ConfigureTcpStream();

            // initialise reader [& marker] from the list of connected devices
            InitialiseConnectedDevices();

            // initiate read if there is a connected Reader
            if (ConnectedReader != null)
                InitiateRead();

        }

        /// <summary>
        /// Dispose TCP stream and release all resources
        /// </summary>
        private void DisposeTcpStream()
        {
            if (BusStream != null && BusStream.IsOpen)
            {
                BusStream.Close();
                BusStream.Dispose();
                // sleep for 100 ms before resuming connection
                Thread.Sleep(100);
            }
        }

        /// <summary>
        /// Read Message event handler to expose read event
        /// </summary>
        public event EventHandler<RampTagReadEventArg> ReadMessage;
    }
}
