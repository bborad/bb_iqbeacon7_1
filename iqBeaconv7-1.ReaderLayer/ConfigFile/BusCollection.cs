﻿/* Name - BusElement
 * Author - Puru Karki
 * Purpose - Provide Element Collection Level Access for Bus File
 * Created Date - 15/10/2014
*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iqBeaconv7_1.ReaderLayer.ConfigFile
{
    public class BusCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Create new Bus Collection programatically [if required]
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new BusElement();
        }

        /// <summary>
        /// Get Existing Bus Collection programatically [if required]
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((BusElement) element).Ip;
        }
    }
}
