﻿/* Name - ConfigurationTools
 * Author - Puru Karki
 * Purpose - To read/write bus configuration setup to the bus.config file
 * Created Date - 15/10/2014
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iqBeaconv7_1.ReaderLayer.Connection;

namespace iqBeaconv7_1.ReaderLayer.ConfigFile
{
    public static class ReaderConfiguration
    {
        /// <summary>
        /// Get bus collection from the bus.config file
        /// </summary>
        /// <returns></returns>
        public static IList<IBusConfig> GetBusConfiguration()
        {
            var busConnections = new List<IBusConfig>();

            // ReSharper disable once SuggestUseVarKeywordEvident
            BusSection section = (BusSection)System.Configuration.ConfigurationManager.GetSection("busConfig");
            if (section != null)
            {
                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (BusElement element in section.Bus)
                {
                    busConnections.Add(new BusElement { Description = element.Description, Ip = element.Ip, Port = element.Port });
                }
            }

            return busConnections;
        }
    }
}
